	<?php

	class Petty_cash_model extends CI_Model 
	{
		public function calculate_balance_brought_forward($date_from)
		{
			$this->db->select('(
	(SELECT SUM(petty_cash_amount) FROM petty_cash WHERE petty_cash_status = 1 AND transaction_type_id = 1 AND petty_cash_date < \''.$date_from.'\')
	-
	(SELECT SUM(petty_cash_amount) FROM petty_cash WHERE petty_cash_status = 1 AND transaction_type_id = 2 AND petty_cash_date < \''.$date_from.'\')
	) AS balance_brought_forward', FALSE); 
			$this->db->where('petty_cash_date < \''.$date_from.'\'');
			$this->db->group_by('balance_brought_forward');
			$query = $this->db->get('petty_cash');
			$row = $query->row();
			return $row->balance_brought_forward;
		}
		
		public function get_petty_cash($where, $table)
		{
			$this->db->select('*');
			// $this->db->join('account', 'petty_cash.account_id = account.account_id', 'left');
			$this->db->where($where);
			$this->db->order_by('petty_cash_date', 'ASC');
			$query = $this->db->get($table);
			
			return $query;
		}
		
		public function get_accounts()
		{
			$this->db->where('account_status = 1');
			$this->db->order_by('account_name');
			$query = $this->db->get('account');
			
			return $query;
		}
		
		public function record_petty_cash()
		{
			$transaction_type_id = $this->input->post('transaction_type_id');
			// var_dump($transaction_type_id); die();
			
			$array = array(
							"petty_cash_date" => $this->input->post('petty_cash_date'),
							"petty_cash_description" => $this->input->post('petty_cash_description'),
							"petty_cash_amount" => $this->input->post('petty_cash_amount'),			
							'created' => date('Y-m-d H:i:s'),
							"created_by" => $this->session->userdata('personnel_id'),
							"modified_by" => $this->session->userdata('personnel_id')
						);

			if($transaction_type_id == 1 OR $transaction_type_id == 3)
			{
				$transaction_type_id = 1;
				$array["transaction_type_id"] = $transaction_type_id;
				$array["account_id"] = $this->input->post('account_id');
				$array["from_account_id"] =$this->input->post('from_account_id');
			}
			else
			{
				$transaction_type_id = 2;
				$array["transaction_type_id"] = $transaction_type_id;
				$array["account_id"] = $this->input->post('from_account_id');
				$array["from_account_id"] =$this->input->post('account_id');
			}
			
			
			if($this->db->insert('petty_cash', $array))
			{
				//if deposit was select, credit the from account with the same amount
				
				
					$credit_data = array(
										"petty_cash_date" => $this->input->post('petty_cash_date'),
										"petty_cash_description" => $this->input->post('petty_cash_description'),
										"petty_cash_amount" => $this->input->post('petty_cash_amount'),
										'created' => date('Y-m-d H:i:s'),
										"created_by" => $this->session->userdata('personnel_id'),
										"modified_by" => $this->session->userdata('personnel_id')
										);
					if($transaction_type_id == 1 OR $transaction_type_id == 3)
					{
						$transaction_type_id = 2;
						$credit_data["transaction_type_id"] = $transaction_type_id;
						$credit_data["account_id"] = $this->input->post('from_account_id');
						$credit_data["from_account_id"] =$this->input->post('account_id');
					}
					else
					{
						$transaction_type_id = 1;
						$credit_data["transaction_type_id"] = $transaction_type_id;
						$credit_data["account_id"] = $this->input->post('account_id');
						$credit_data["from_account_id"] =$this->input->post('from_account_id');
					}
					if($this->db->insert('petty_cash', $credit_data))
					{
					}
				
				return TRUE;
			}
			
			else
			{
				return FALSE;
			}
		}
		public function get_account_name($from_account_id)
		{
			$account_name = '';
			$this->db->select('account_name');
			$this->db->where('account_id = '.$from_account_id);
			$query = $this->db->get('account');
			
			$account_details = $query->row();
			$account_name = $account_details->account_name;
			
			return $account_name;
		}
		public function get_total_deposited($account_id)
		{
			$amount_deposited = 0;
			$this->db->select('SUM(petty_cash_amount) AS total_deposited');
			$this->db->where('transaction_type_id = 1 AND account_id = '.$account_id);
			
			$query = $this->db->get('petty_cash');
			$deposits_row = $query->row();
			$amount_deposited = $deposits_row->total_deposited;
			
			return $amount_deposited;
		}
		public function get_total_spent($account_id)
		{
			$expenditure = 0 ;
			$this->db->select('SUM(petty_cash_amount) AS total_spent');
			$this->db->where('transaction_type_id = 2 AND account_id = '.$account_id);
			
			$query = $this->db->get('petty_cash'); 
			$expenditure_row = $query->row();
			$expenditure = $expenditure_row->total_spent;
			
			return $expenditure;
		}
		public function get_all_cash_accounts($table, $where, $config, $page, $order, $order_method)
		{
			//retrieve all accounts
			$this->db->from($table);
			$this->db->select('*');
			$this->db->where($where);
			$this->db->order_by($order, $order_method);
			$query = $this->db->get('', $config, $page);
			
			return $query;
		}
		public function deactivate_account($account_id)
		{
			$this->db->where('account_id = '.$account_id);
			if($this->db->update('account',array('account_status'=>0)))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		public function activate_account($account_id)
		{
			$this->db->where('account_id = '.$account_id);
			if($this->db->update('account',array('account_status'=>1)))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		public function get_account($account_id)
		{
			$this->db->select('*');
			$this->db->where('account_id = '.$account_id);
			$query = $this->db->get('account');
			
			return $query->row();
		}
		public function update_account($account_id)
		{
			$account_data = array(
						'account_name'=>$this->input->post('account_name'),
						'account_type_id'=>$this->input->post('account_type_id'),
						'account_opening_balance'=>$this->input->post('account_balance')
						);
			$this->db->where('account_id = '.$account_id);
			if($this->db->update('account', $account_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		public function add_account()
		{
			$account = array(
						'account_name'=>$this->input->post('account_name'),
						'account_opening_balance'=>$this->input->post('account_balance'),
						'account_type_id'=>$this->input->post('account_type_id'),
	                    'account_status'=>$this->input->post('account_status')
						);
			if($this->db->insert('account',$account))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		public function get_account_opening_bal($account)
		{
			$opening_bal = 0;
			
			$this->db->select('account_opening_balance');
			$this->db->where('account_id = '.$account);
			$query = $this->db->get('account');
			
			$bal = $query->row();
			$opening_bal = $bal->account_opening_balance;

			return $opening_bal;
			
		}
		public function get_total_opening_bal()
		{
			$opening_bal = 0;
			
			$this->db->select('SUM(account_opening_balance) AS total_opening_bal');
			$query = $this->db->get('account');
			
			$bal = $query->row();
			$opening_bal = $bal->total_opening_bal;

			return $opening_bal;
		}
		public function delete_petty_cash($petty_cash_id)
		{
			$this->db->where('petty_cash_id',$petty_cash_id);
			$query = $this->db->get('petty_cash');

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$petty_cash_amount = $value->petty_cash_amount;
					$petty_cash_date = $value->petty_cash_date;
					$account_id = $value->account_id;
					$transaction_type_id = $value->transaction_type_id;
					$from_account_id = $value->from_account_id;
					$petty_cash_description = $value->petty_cash_description;

				}
			}


				$update_array = array(
						'petty_cash_delete'=>1
					);
				$this->db->where('petty_cash_id', $petty_cash_id);
				if($this->db->update('petty_cash', $update_array))
				{

					if($transaction_type_id == 1)
		            {
		            	$array = array(
		             					'petty_cash_amount'=>$petty_cash_amount,
		             					'petty_cash_date'=>$petty_cash_date,
		             					'account_id'=>$from_account_id,
		             					'from_account_id'=>$account_id,
		             					'petty_cash_description'=>$petty_cash_description,
		             					'transaction_type_id'=>2
		             				   );
		            }
		            else
		            {

		            	$array = array(
		             					'petty_cash_amount'=>$petty_cash_amount,
		             					'petty_cash_date'=>$petty_cash_date,
		             					'account_id'=>$account_id,
		             					'from_account_id'=>$from_id,
		             					'petty_cash_description'=>$petty_cash_description,
		             					'transaction_type_id'=>1
		             				   );

		            }

		            $update_array2 = array(
											'petty_cash_delete'=>1
										);
					$this->db->where($array);
					if($this->db->update('petty_cash', $update_array2))
					{
						return TRUE;	
					}
					else
					{
						return FALSE;
					}
					
				}
				else{
					return FALSE;
				}
		}
		 public function get_type()
		
		  {
			//retrieve all users
			$this->db->from('account_type');
			$this->db->select('*');
			$this->db->where('account_type_id > 0 ');
			$query = $this->db->get();
			
			return $query;    	
	 
	     }
	}
	?>