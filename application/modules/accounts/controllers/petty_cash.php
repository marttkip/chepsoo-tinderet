<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/accounts/controllers/accounts.php";

class Petty_cash extends accounts 
{
	function __construct()
	{
		parent:: __construct();
	}
	
	public function index()
	{
		$date_from = NULL;
		$where = 'petty_cash.transaction_type_id = transaction_type.transaction_type_id AND petty_cash.petty_cash_status = 1 AND petty_cash.petty_cash_delete = 0 AND petty_cash.account_id = account.account_id ';
		$table = 'petty_cash,transaction_type,account';

		$title = 'Account Details';
		$search = $this->session->userdata('accounts_search');
		$search_title = $this->session->userdata('accounts_search_title');
		$from = $this->session->userdata('date_from');
		$account = $this->session->userdata('account_id');//echo $account;die();
		if(!empty($search))
		{
			$where.= $search;
		}
		else
		{
			$where.=' AND account.account_name = "Petty Cash"';
		}
		if(!empty($search_title))
		{
			$title = $search_title;
		}
		if(!empty($from))
		{
			$date_from = $from;
		}
		//var_dump($where); die();
		$v_data['balance_brought_forward'] = $this->petty_cash_model->calculate_balance_brought_forward($date_from);
		
		$v_data['date_from'] = $from;
		$v_data['date_to'] = $date_to;
		$v_data['account'] = $account;
		$v_data['accounts'] = $this->petty_cash_model->get_accounts();
		$v_data['query'] = $this->petty_cash_model->get_petty_cash($where, $table);
		$v_data['title'] = $title;
		$data['title'] = 'Accounting';
		$data['content'] = $this->load->view('petty_cash/statement', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function record_petty_cash()
	{
		$this->form_validation->set_rules('transaction_type_id', 'Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('account_id', 'Account', 'xss_clean');
		$this->form_validation->set_rules('from_account_id', 'From Account', 'xss_clean');
		$this->form_validation->set_rules('petty_cash_description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('petty_cash_amount', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('petty_cash_date', 'Transaction date', 'required|xss_clean');
		
		// credit or debit
		$transaction_type_id = $this->input->post('transaction_type_id');

		if ($this->form_validation->run())
		{
			$to_account = $this->input->post('account_id');
			$from_account =$this->input->post('from_account_id');

			
			if($this->petty_cash_model->record_petty_cash())
			{
				$this->session->set_userdata('success_message', 'Record saved successfully');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Unable to save. Please try again');
			}
			
		}
		
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}
		
		redirect('accounts/petty-cash');
	}
	
	public function search_petty_cash()
	{
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		$account_id = $this->input->post('account_id');
		$account_where = '';
		$date_where = '';
		$search_title = '';
		if(!empty($account_id))
		{

			$this->db->where('account_id',$account_id);
			$query = $this->db->get('account');
			$account_name = 'Petty Cash';
			if($query->result() > 0 )
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$account_name = $value->account_name;
				}
			}
			$account_where = ' AND petty_cash.account_id = '.$account_id;
			$search_title = 'Petty based on account '.$account_name;
		}
		if(!empty($date_from) && !empty($date_to))
		{
			$date_where = ' AND (petty_cash.petty_cash_date >= \''.$date_from.'\' AND petty_cash.petty_cash_date <= \''.$date_to.'\')';
			//$where .= ' AND petty_cash.petty_cash_date BETWEEN \''.$date_from.'\' AND \'petty_cash.petty_cash_date <= '.$date_to.'\')';
			$search_title = 'Petty cash from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}
		
		if(!empty($date_from))
		{
			$date_where = ' AND petty_cash.petty_cash_date >= \''.$date_from.'\'';
			$search_title = 'Petty cash of '.date('jS M Y', strtotime($date_from)).' ';
		}
		
		else if(!empty($date_to))
		{
			$date_where = ' AND petty_cash.petty_cash_date <= \''.$date_to.'\'';
			$search_title = 'Petty cash of '.date('jS M Y', strtotime($date_to)).' ';
		} 
		$search = $account_where.$date_where;
		$this->session->set_userdata('accounts_search', $search);
		$this->session->set_userdata('accounts_search_title', $search_title);
		$this->session->set_userdata('date_from',$date_from);
		$this->session->set_userdata('date_to',$date_to);
		$this->session->set_userdata('account_id',$account_id);
		
		redirect('accounts/petty-cash');
		/*if(!empty($date_from) && !empty($date_to) && !empty($account_id))
		{
			$where = ' AND petty_cash.account_id = '.$account_id. ' AND (petty_cash.petty_cash_date >= \''.$date_from.'\' AND petty_cash.petty_cash_date <= \''.$date_to.'\')';
			//$where .= ' AND petty_cash.petty_cash_date BETWEEN \''.$date_from.'\' AND \'petty_cash.petty_cash_date <= '.$date_to.'\')';
			$search_title = 'Petty cash from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).'  Account '.$account_id;
		}*/
		
	
	}
	
	public function print_petty_cash()
	{
		$date_from = NULL;
		$where = 'petty_cash.transaction_type_id = transaction_type.transaction_type_id AND petty_cash.petty_cash_status = 1 AND petty_cash.petty_cash_delete = 0 AND petty_cash.account_id = account.account_id ';
		$table = 'petty_cash, transaction_type';
		$title = 'Account Details';
		$search = $this->session->userdata('accounts_search');
		$search_title = $this->session->userdata('accounts_search_title');
		$from = $this->session->userdata('date_from');
		$account = $this->session->userdata('account_id');//echo $account;die();
		if(!empty($search))
		{
			$where.= $search;
		}
		else
		{
			$where .=' AND account.account_name = "Petty Cash"';
		}
		if(!empty($search_title))
		{
			$title = $search_title;
		}
		if(!empty($from))
		{
			$date_from = $from;
		}
		
		$v_data['balance_brought_forward'] = $this->petty_cash_model->calculate_balance_brought_forward($date_from);
		
		$v_data['contacts'] = $this->site_model->get_contacts();
		$v_data['date_from'] = $date_from;
		$v_data['date_to'] = $date_to;
		$v_data['account'] = $account;
		$v_data['query'] = $this->petty_cash_model->get_petty_cash($where, $table);
		$v_data['title'] = $search_title;
		$this->load->view('petty_cash/print_petty_cash', $v_data);
	}
	public function account_balances($order = 'account_name',$order_method ='ASC')
	{
		$where = 'account_id > 0 AND account_type.account_type_id = account.account_type_id';
		$table = 'account,account_type';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/general-journal-entries/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->petty_cash_model->get_all_cash_accounts($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('petty_cash/all_accounts', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function deactivate_account($account_id)
	{
		if($this->petty_cash_model->deactivate_account($account_id))
		{
			$this->session->set_userdata('success_message', 'Account deactivated successfully');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Account deactivation failed');
		}
		
		redirect('accounts/general-journal-entries');
	}
	public function activate_account($account_id)
	{
		if($this->petty_cash_model->activate_account($account_id))
		{
			$this->session->set_userdata('success_message', 'Account activated successfully');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Account activation failed');
		}
		
		redirect('accounts/general-journal-entries');
	}
	public function edit_account($account_id)
	{
		//form validation
		$this->form_validation->set_rules('account_name', 'Name','required|xss_clean');
		$this->form_validation->set_rules('account_balance', 'Opening Balance','required|xss_clean');
		$this->form_validation->set_rules('account_type_id', 'Account type','required|xss_clean');

		
		if ($this->form_validation->run())
		{
			//update order
			if($this->petty_cash_model->update_account($account_id))
			{
				$this->session->set_userdata('success_message', 'Account updated successfully');
				redirect('accounts/general-journal-entries');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update account. Please try again');
			}
		}
		
		//open the add new order
		$data['title'] = $v_data['title']= 'Edit Account';
		$v_data['types'] = $this->petty_cash_model->get_type();
		
		//select the order from the database
		$query = $this->petty_cash_model->get_account($account_id);
		$v_data['query'] = $query;
		$data['content'] = $this->load->view('petty_cash/edit_account', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_account()
	{
		//form validation
		$this->form_validation->set_rules('account_name', 'Name','required|xss_clean');
		$this->form_validation->set_rules('account_balance', 'Opening Balance','required|xss_clean');
		$this->form_validation->set_rules('account_type_id', 'Account_type','required|xss_clean');
		
		if ($this->form_validation->run())
		{
			//update order
			if($this->petty_cash_model->add_account())
			{
				$this->session->set_userdata('success_message', 'Account updated successfully');
				redirect('accounts/general-journal-entries');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update account. Please try again');
			}
		}
		
		//open the add new order
		$v_data['types'] = $this->petty_cash_model->get_type();
		$data['title'] = $v_data['title']= 'Add Account';
		$data['content'] = $this->load->view('petty_cash/add_account', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function close_search()
	{
		$this->session->unset_userdata('accounts_search', $search);
		$this->session->unset_userdata('accounts_search_title', $search_title);
		$this->session->unset_userdata('date_from',$date_from);;
		$this->session->unset_userdata('account_id',$account_id);;
		redirect('accounts/petty_cash');
	}

	public function delete_petty_cash($petty_cash_id)
    {
		//delete creditor
		
		$this->petty_cash_model->delete_petty_cash($petty_cash_id);
		$this->session->set_userdata('success_message', 'Debit or Credit has been deleted');
		redirect('accounts/petty-cash');
    }	
}
?>