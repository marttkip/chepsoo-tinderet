
 <section class="panel ">
	<header class="panel-heading">
		<div class="panel-title">
		<strong>Name:</strong> <?php echo $patient_surname.' '.$patient_othernames;?>. <strong> Patient Visit: </strong><?php echo $visit_type_name;?>. 
		

		</div>
		<div class="pull-right">
			  <?php
				if($inpatient > 0)
				{
					?>
					<a href="<?php echo site_url();?>queues/inpatient-queue" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-left"></i> Back to Inpatient Queue</a>
			
					<?php
				}
				else
				{
					?>
					<a href="<?php echo site_url();?>queues/walkins" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-left"></i> Back to Walkins Queue</a>
			
					<?php
				}
				?>
		</div>
	</header>
	
	<!-- Widget content -->
	
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
				  echo '<div class="alert alert-danger">'.$error.'</div>';
				  $this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
				  echo '<div class="alert alert-success">'.$success.'</div>';
				  $this->session->unset_userdata('success_message');
				}
			 ?>
			</div>
		</div>
		
        
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12">
					<section class="panel panel-featured panel-featured-info">
				        <header class="panel-heading">
				            <h2 class="panel-title">Prescription services</h2>
				        </header>
				        <div class="panel-body">
				            <div class="col-lg-8 col-md-8 col-sm-8">
				              <div class="form-group">
				              	<label class="col-md-2 control-label">Drugs: </label>
            
            						<div class="col-md-10">
						                <select id='service_id_item' name='service_charge_id' class='form-control custom-select ' onchange="get_service_charge_amount(this.value)">
						                  <option value=''>None - Please Select a Drugs to prescribe</option>
						                  <?php echo $services_list;?>
						                </select>
						         </div>
				              </div>
				            </div>
				            <input type="hidden" name="charge" id="drug-charge" >
				            <div class="col-lg-4 col-md-4 col-sm-4">
							  <div class="form-group">
								  <button type='submit' class="btn btn-sm btn-success"  onclick="parse_drug_charge(<?php echo $visit_id;?>);"> Add Drug</button>
							  </div>
							</div>
				            <div id="prescription_view"></div>
 							<div id="visit_prescription"></div>
				        </div>
				    </section>
				

			
				</div>
				<!-- END OF THE SPAN 7 -->
			</div>
		</div>


		<div class="row">
			<div class="col-md-12 center-align">
				<?php
				$query = $this->reception_model->get_visit($visit_id);
				$closed = 0;
				if($query->num_rows() == 1)
				{
					foreach ($query->result() as $key => $value) {
						# code...
						$closed = $value->closed;
					}
				}
				if($closed == 1)
				{
					?>
						<a href="<?php echo site_url();?>pharmacy/close_visit/<?php echo $visit_id?>" class='btn btn-info btn-sm' onclick="return confirm('Do you want to post sale ?')" > Post Sale </a>
					<?php
				}
				else
				{
					?>
					<a href="<?php echo site_url();?>pharmacy/send_to_walkin_accounts/<?php echo $visit_id?>" class='btn btn-success btn-sm'  onclick="return confirm('Do you send to accounts ?')" > Send to accounts </a>
					<?php

				}
				?>
				
				
			</div>
		</div>
			
	
	</div>
</section>
  <!-- END OF ROW -->
<script type="text/javascript">

 
   $(function() {
       $("#service_id_item").customselect();

   });
   $(document).ready(function(){
   		display_inpatient_prescription(<?php echo $visit_id;?>,1);
   });

</script>
  <script type="text/javascript">
   function get_drug_to_prescribe(visit_id)
   {
   var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var drug_id = document.getElementById("drug_id").value;
   
     var url = "<?php echo site_url();?>pharmacy/inpatient_prescription/"+visit_id+"/"+drug_id+"/2";
   
      if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
               var prescription_view = document.getElementById("prescription_view");
              
               document.getElementById("prescription_view").innerHTML=XMLHttpRequestObject.responseText;
                prescription_view.style.display = 'block';
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   
   
   }
    function parse_drug_charge(visit_id)
	{
		var service_charge_id = document.getElementById("service_id_item").value;
		prescibe_drug(service_charge_id, visit_id);	    
	}
	function prescibe_drug(service_charge_id, visit_id){
    
	   var quantity = 1;
	   var x = 1;
	   var duration = 1;
	   var consumption = 1;
	   var charge = $('#drug-charge').val();;
	   var units_given = 1;
	   var input_total_units = 1;
	   
	   var url = "<?php echo base_url();?>pharmacy/add_pharmacy_charge/"+service_charge_id+"/"+visit_id;
	   
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given,service_charge_id: service_charge_id,input_total_units: input_total_units},
	   dataType: 'text',
	   success:function(data){
	     // window.alert(data.result);
	      display_inpatient_prescription(visit_id,1);
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   	 display_inpatient_prescription(visit_id,1);
	   }
	   });
	   // display_inpatient_prescription(visit_id,1);
	   return false;
	}
    function display_inpatient_prescription(visit_id,module){
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"pharmacy/display_inpatient_prescription/"+visit_id+"/"+module;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("visit_prescription").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }
    function delete_prescription(prescription_id, visit_id,visit_charge_id,module)
   {
   var res = confirm('Are you sure you want to delete this prescription ?');
   
   if(res)
   {
     var XMLHttpRequestObject = false;
     
     if (window.XMLHttpRequest) {
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
     
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"pharmacy/delete_inpatient_prescription/"+prescription_id+"/"+visit_id+"/"+visit_charge_id+"/"+module;
     // alert(url);
     if(XMLHttpRequestObject) {
       
       XMLHttpRequestObject.open("GET", url);
       
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           
            display_inpatient_prescription(visit_id,1);
          
         }
       }
       XMLHttpRequestObject.send(null);
     }
   }
   }



   function button_update_prescription(visit_id,visit_charge_id,prescription_id,module)
   {
   var quantity = $('#quantity'+prescription_id).val();
   var x = $('#x'+prescription_id).val();
   var duration = $('#duration'+prescription_id).val();
   var consumption = $('#consumption'+prescription_id).val();
   var url = "<?php echo base_url();?>pharmacy/update_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module;
   
   
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption},
   dataType: 'text',
   success:function(data){
   
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   display_inpatient_prescription(visit_id,1);
   return false;
   }
   
  function dispense_prescription(visit_id,visit_charge_id,prescription_id,module)
	{

	  var quantity =  $('#quantity'+prescription_id).val();
	  var x = $('#x'+prescription_id).val();
	  var duration = $('#duration'+prescription_id).val();
	  var consumption = $('#consumption'+prescription_id).val();
	  var charge = $('#charge'+prescription_id).val();
	  var units_given = $('#units_given'+prescription_id).val();
	  
	  var url = "<?php echo site_url();?>pharmacy/dispense_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module+"/"+quantity;
	 // alert(url);
	  $.ajax({
	  type:'POST',
	  url: url,
	  data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given},
	  dataType: 'text',
	  success:function(data){
	  	var data = jQuery.parseJSON(data);
	  	display_inpatient_prescription(visit_id,1);
	  	if(data.status == 1)
	  	{
	    	window.alert(data.result);
	  	}
	  	else
	  	{
	  		var res = confirm(data.result);  
			if(res)
			{
				var url = "<?php echo site_url();?>pharmacy/borrow_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module+"/"+quantity;
				$.ajax({
				type:'POST',
				url: url,
				data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given},
				dataType: 'json',
				success:function(data){

					display_inpatient_prescription(visit_id,1);					
					window.alert(data.result);
				},
				error: function(xhr, status, error) {
				alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

				}
				});
				return false;
			}

	  	}
	  },
	  error: function(xhr, status, error) {
	  alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	  }
	  });
	  return false;
	}


    function pass_prescription()
   {

   var quantity = document.getElementById("quantity_value").value;
   var x = document.getElementById("x_value").value;
   var dose_value = document.getElementById("dose_value").value;
   var duration = 1;//document.getElementById("duration_value").value;
   var consumption = document.getElementById("consumption_value").value;
   var number_of_days = document.getElementById("number_of_days_value").value;
   var service_charge_id = document.getElementById("drug_id").value;
   var visit_id = document.getElementById("visit_id").value;
   var input_total_units = document.getElementById("input-total-value").value;
   var module = document.getElementById("module").value;
   var passed_value = document.getElementById("passed_value").value;
   var type_of_drug = document.getElementById("type_of_drug").value;
   
   var url = "<?php echo base_url();?>pharmacy/prescribe_prescription";
   
  
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,module: module,passed_value:passed_value,input_total_units:input_total_units,dose_value: dose_value,type_of_drug: type_of_drug},
   dataType: 'text',
   success:function(data){
   
   var prescription_view = document.getElementById("prescription_view");
   prescription_view.style.display = 'none';
   display_inpatient_prescription(visit_id,0);
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   
   return false;
   }


  function check_frequency_type()
   {
   
     var x = document.getElementById("x_value").value;
     var type_of_drug = document.getElementById("type_of_drug").value;
       
     var number_of_days = document.getElementById("number_of_days_value").value;
     var quantity = document.getElementById("quantity_value").value;
     var dose_value = document.getElementById("dose_value").value;

    var service_charge_id = document.getElementById("drug_id").value;
    var visit_id = document.getElementById("visit_id").value;
    var consumption = document.getElementById("consumption_value").value;

     if(x == "" || x == 0)
     {

       // alert("Please select the frequency of the medicine");
       x = 1;
     }
     
     if(number_of_days == "" || number_of_days == 0)
     {
        number_of_days = 1;
     }
     
         
      var url = "<?php echo base_url();?>pharmacy/get_values";
       $.ajax({
       type:'POST',
       url: url,
       data:{quantity: quantity, x: x, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,dose_value:dose_value,consumption: consumption,type_of_drug: type_of_drug},
       dataType: 'text',
       success:function(data){
          var data = jQuery.parseJSON(data);

          var amount = data.amount;
          var frequency = data.frequency;
           var item = data.item;

          // {"message":"success","amount":"35","frequency":"5"}

          if(type_of_drug == 3)
          {
           
            var total_units = number_of_days * frequency * quantity;
            var total_amount =  number_of_days * frequency * amount * quantity;
          }
          else
          {

             var total_units =   quantity;
             var total_amount =   amount * quantity;
          }
        

        // document.getElementById("total_units").innerHTML = "<h2>"+ amount +" units</h2>";
         $( "#total_units" ).html("<h2>"+ total_units +" units</h2>");
         $( "#total_amount" ).html("<h3>Ksh. "+ total_amount +"</h3>");
         $( "#item_description" ).html("<p> "+ item +"</p>");

         document.getElementById("input-total-value").value = total_units;
         // document.getElementById("total_amount").innerHTML = "<h3>Ksh. "+ frequency +" units</h3>";


       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
   
        
     
  }

  function get_service_charge_amount(service_charge_id)
  {
  	   var url = "<?php echo base_url();?>pharmacy/get_drug_price/"+service_charge_id;
  	   // alert(url);
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{service_charge_id: service_charge_id},
	   dataType: 'text',
	   success:function(data){
	   	 var data = jQuery.parseJSON(data);
         var amount = data.amount
         // alert(amount);
	   	 document.getElementById("drug-charge").value = amount;
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
	   
	   return false;
  }
  function undispense_prescription(visit_id,visit_charge_id,prescription_id,module)
	{

	  var quantity =  $('#quantity'+prescription_id).val();
	  var x = $('#x'+prescription_id).val();
	  var duration = $('#duration'+prescription_id).val();
	  var consumption = $('#consumption'+prescription_id).val();
	  var charge = $('#charge'+prescription_id).val();
	  var units_given = $('#units_given'+prescription_id).val();
	  
	  var url = "<?php echo site_url();?>pharmacy/undispense_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module+"/"+quantity;
	 // alert(url);
	  $.ajax({
	  type:'POST',
	  url: url,
	  data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given},
	  dataType: 'json',
	  success:function(data){
	  	display_inpatient_prescription(visit_id,1);
	    window.alert(data.result);
	  },
	  error: function(xhr, status, error) {
	  alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	  }
	  });
	  display_inpatient_prescription(visit_id,1);
	  return false;
	}
   
   function update_prescription_values(visit_id,visit_charge_id,prescription_id,module)
    {
      
       //var product_deductions_id = $(this).attr('href');
       var quantity = $('#quantity'+prescription_id).val();
       var x = $('#x'+prescription_id).val();
       var duration = $('#duration'+prescription_id).val();
       var consumption = $('#consumption'+prescription_id).val();


       var url = "<?php echo base_url();?>pharmacy/update_prescription/"+visit_id+'/'+visit_charge_id+'/'+prescription_id+'/'+module;
  
        //window.alert(data_url);
		  $.ajax({
		  type:'POST',
		  url: data_url,
		  data:{quantity: quantity, x: x, duration: duration,consumption: consumption},
		  dataType: 'text',
           success:function(data){
            
            window.alert(data.result);
            if(module == 1){
				window.location.href = "<?php echo base_url();?>pharmacy/prescription1/"+visit_id+"/1'";
			
			}else{
				window.location.href = "<?php echo base_url();?>pharmacy/prescription1/"+visit_id+"";
			}
           },
           error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
           
           }
        });
        return false;
     }
  </script>