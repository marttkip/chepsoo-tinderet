<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lab_Walkins  extends MX_Controller
{	
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		$this->load->model('pharmacy/pharmacy_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('reception/reception_model');
		$this->load->model('nurse/nurse_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('laboratory/lab_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('reception/database');
		$this->load->model('administration/personnel_model');
		$this->load->model('walkins/walkins_model');
		$this->load->model('inventory_management/inventory_management_model');

		$this->csv_path = realpath(APPPATH . '../assets/csv');
		
		/*$this->load->model('auth/auth_model');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}*/
	}


	public function index()
	{	
		$visit_id = $this->session->userdata('visit_id_lab');

		$v_data['title'] = '';
		$accounts_mode = FALSE;
  		if($visit_id)
  		{
			$v_data['visit_id'] = $visit_id;
			$patient = $this->reception_model->patient_names2(NULL, $visit_id);
			$v_data['patient_type'] = $patient['patient_type'];
			$v_data['patient_othernames'] = $patient['patient_othernames'];
			$v_data['patient_surname'] = $patient['patient_surname'];
			$v_data['patient_type_id'] = $patient['visit_type_id'];
			$v_data['account_balance'] = $patient['account_balance'];
			$v_data['visit_type_name'] = $patient['visit_type_name'];
			$v_data['patient_id'] = $patient['patient_id'];
			$v_data['inpatient'] = $patient['inpatient'];
			$patient_date_of_birth = $patient['patient_date_of_birth'];
			$age = $this->reception_model->calculate_age($patient_date_of_birth);
			$visit_date = $this->reception_model->get_visit_date($visit_id);
			$gender = $patient['gender'];
			$visit_date = date('jS M Y',strtotime($visit_date));
			$v_data['age'] = $age;
			$v_data['visit_date'] = $visit_date;
			$v_data['gender'] = $gender;

			$v_data['title'] = '<strong> Number: </strong> CID'.$visit_id;
			$accounts_mode = FALSE;
			$v_data['accounts_mode'] = $accounts_mode;
		}
		else
		{
			$this->db->where('patient_id = 293 AND close_card = 0');
			$query = $this->db->get('visit');
				// var_dump($query->num_rows()); die();

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value_item) {
					# code...
					$patient_id = $value_item->patient_id;
					$visit_id = $value_item->visit_id;
				}

				$v_data = array('patient_id'=>$patient_id,'visit_id'=>$visit_id);		
				$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
				$v_data['visit_types_rs'] = $this->reception_model->get_visit_types();
				$patient = $this->reception_model->get_patient_data($patient_id);
				
				if($patient->num_rows() > 0)
				{
					$patient = $patient->row();
					$patient_othernames = $patient->patient_othernames;
					$patient_surname = $patient->patient_surname;	
				}
				else
				{
					$patient_surname ='';
					$patient_othernames = '';
				}
				

				$v_data['doctor'] = $this->reception_model->get_providers();

				
				$v_data['title'] = $patient_othernames.' '.$patient_surname;




				$order = 'service_charge.service_charge_name';
				$where = 'service_charge.service_id = service.service_id AND service.service_name <> "Laboratory" AND service.service_delete = 0 AND service_charge.service_charge_status = 1 AND  service_charge.service_charge_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id = 1';

				$table = 'service_charge,visit_type,service';
				$config["per_page"] = 0;
				$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

				$rs9 = $procedure_query->result();
				$procedures = '';
				foreach ($rs9 as $rs10) :


				$procedure_id = $rs10->service_charge_id;
				$proced = $rs10->service_charge_name;
				$visit_type = $rs10->visit_type_id;
				$visit_type_name = $rs10->visit_type_name;

				$stud = $rs10->service_charge_amount;

				    $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

				endforeach;

				$v_data['services_list'] = $procedures;



				$order = 'service.service_name';
				$where = 'service.service_name <> "Laboratory" AND service_status = 1';

				$table = 'service';
				$service_query = $this->nurse_model->get_other_procedures($table, $where, $order);

				$rs9 = $service_query->result();
				$services_items = '';
				foreach ($rs9 as $rs11) :


					$service_id = $rs11->service_id;
					$service_name = $rs11->service_name;

					$services_items .="<option value='".$service_id."'>".$service_name."</option>";

				endforeach;

				$v_data['services_items'] = $services_items;
				$v_data['close_page'] = 1;
				$accounts_mode = TRUE;
			}
			else
			{
				$accounts_mode = FALSE;

			}

			$v_data['accounts_mode'] = $accounts_mode;
			
		}



			$v_data['module'] = 1;

			
			
		 
		$lab_test_order = 'service_charge_name';

		$lab_test_where = 'service_charge.service_charge_status = 1 AND service_charge.service_charge_delete = 0 AND lab_test.lab_test_delete = 0 AND service_charge.service_charge_name = lab_test.lab_test_name AND lab_test_class.lab_test_class_id = lab_test.lab_test_class_id  AND service_charge.service_id = service.service_id AND (service.service_name = "Lab" OR service.service_name = "lab" OR service.service_name = "Laboratory" OR service.service_name = "laboratory" OR service.service_name = "Laboratory test")  AND  service_charge.visit_type_id = 1';
		    
		$lab_test_table = '`service_charge`, lab_test_class, lab_test, service';

		$lab_test_query = $this->lab_model->get_inpatient_lab_tests($lab_test_table, $lab_test_where, $lab_test_order);

		$rs11 = $lab_test_query->result();
		$lab_tests = '';
		foreach ($rs11 as $lab_test_rs) :


		  $lab_test_id = $lab_test_rs->service_charge_id;
		  $lab_test_name = $lab_test_rs->service_charge_name;

		  $lab_test_price = $lab_test_rs->service_charge_amount;

		  $lab_tests .="<option value='".$lab_test_id."'>".$lab_test_name." KES.".$lab_test_price."</option>";

		endforeach;

		$v_data['lab_tests'] = $lab_tests;

		$data['content'] = $this->load->view('lab_walkins', $v_data, TRUE);
		
		$data['title'] = 'Laboratory Walkins';
		$data['sidebar'] = 'pharmacy_sidebar';
		$this->load->view('admin/templates/general_page', $data);
	}

	public function create_walkin_visit()
	{
		$visit_id = $this->session->userdata('visit_id_lab');

  		if($visit_id)
  		{
			$this->session->set_userdata('error_message','Sorry, seems like there is another visit not completed');
		}
		else
		{
			$patient_name = $this->input->post('patient_name');
			$gender = $this->input->post('gender');
			$patient_age = $this->input->post('age');



			$this->form_validation->set_rules('patient_name', 'Name', 'required|xss_clean');
			$this->form_validation->set_rules('gender', 'Gender', 'required|trim|xss_clean');
			$this->form_validation->set_rules('age', 'Age', 'required|trim|xss_clean');
		
			
			//if form conatins invalid data
			if ($this->form_validation->run())
			{

				// / create new patient 
				$item_array['patient_surname'] = $patient_name;
				$item_array['patient_othernames'] = '';
				$item_array['patient_date'] = date('Y-m-d H:i:s');
				$item_array['patient_type'] = 1;
				$item_array['patient_age'] = $patient_age;
				$item_array['gender_id'] = $gender;
				$this->db->insert('patients',$item_array);
				$patient_id = $this->db->insert_id();

				if(!$patient_id)
				{
					$this->session->set_userdata('error_message','There is an incomplete walkin. Please try again after a short while');
				}
				else
				{
					$visit_data = array(
											"branch_code" => $this->session->userdata('branch_code'),
											"visit_date" => date('Y-m-d'),
											"patient_id" => $patient_id,
											"personnel_id" => 0,
											"insurance_limit" => '',
											"patient_insurance_number" => '',
											"visit_type" => 1,
											"time_start"=> date('H:i:s'),
											"close_card"=>0,
										);


					$this->db->insert('visit', $visit_data);
					$visit_id = $this->db->insert_id();
					$this->session->set_userdata('success_message','Visit created successfully');
					$this->session->set_userdata('visit_id_lab',$visit_id);

				}
			}
			else
			{
				$this->session->set_userdata("error_message","Could not add patient. Please try again");
			}
			
		}
		redirect('lab-walkins');
	}
	public function send_to_accounts()
	{
		$this->session->unset_userdata('visit_id');
		redirect('lab-walkins');
	}


	public function get_patient_details_header($visit_id)
	{

		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		$v_data['going_to'] = $this->accounts_model->get_going_to($visit_id);
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$patient_othernames = $patient['patient_othernames'];
		$patient_surname= $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$account_balance= $patient['account_balance'];
		$visit_type_name= $patient['visit_type_name'];
		$v_data['patient_id'] = $patient['patient_id'];
		$v_data['inpatient'] = $inpatient = $patient['inpatient'];
		$payments_value = $this->accounts_model->total_payments($visit_id);
		$invoice_total = $this->accounts_model->total_invoice($visit_id);
		$balance = $this->accounts_model->balance($payments_value,$invoice_total);

		if($inpatient == 1)
		{
			$visit_discharge = '<a target="_blank" onclick="close_visit('.$visit_id.')" class="btn btn-sm btn-danger pull-right" style="margin-top:-25px;" ><i class="fa fa-folder"></i> Discharge Patient</a>';
		}
		else
		{
			$visit_discharge = '<a class="btn btn-sm btn-danger pull-right" href="'.site_url().'end-visit/'.$visit_id.'"  style="margin-top:-25px;" onclick="return confirm(\'Do you want to end this visit ? \');" ><i class="fa fa-folder"></i> End Visit</a>';
		}

		$title = '<h2 class="panel-title"><strong>Visit: </strong>'.$visit_type_name.'.<strong> Total: </strong> Kes '.number_format($account_balance, 2).' <strong> Current: </strong> Kes '.$balance.'</h2>
				<div class="pull-right">
					   

					'.$visit_discharge.'
					<a href="'.site_url().'accounts/print_invoice_new/'.$visit_id.'" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px; margin-right:2px;" ><i class="fa fa-print"></i> Current Invoice</a>
				</div>';

		echo $title;
	}

	public function get_patient_receipt($visit_id,$page=NULL)
	{
		if($page == NULL)
		{
			$page = 0;
		}
		// $visit_id = 23;
		$table= 'payments, payment_method';
		$where="payments.cancel = 0 AND payment_method.payment_method_id = payments.payment_method_id AND payments.visit_id =".$visit_id;
		$config["per_page"] = $v_data['per_page'] = $per_page = 10;
		if($page==0)
		{

			$counted = 0;
		}
		else if($page > 0)
		{

			$counted = $per_page*$page;
		}

		$v_data['page'] = $page;
		$v_data['visit_id'] = $visit_id;
		$page = $counted;
		$v_data['total_rows'] = $this->reception_model->count_items($table, $where);

		$query = $this->accounts_model->get_all_visits_payments_items($table, $where, $config["per_page"], $page);



		$v_data['receipts_items'] = $query;

		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();

		$this->load->view('payments_made',$v_data);
	}
	public function view_patient_bill($visit_id,$page=NULL)
	{

		if($page == NULL)
		{
			$page = 0;
		}
		$table= 'visit_charge, service_charge, service';
		$where='visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = '.$visit_id.' AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id ';

		$config["per_page"] = $v_data['per_page'] = $per_page = 10;
		if($page==0)
		{

			$counted = 0;
		}
		else if($page > 0)
		{

			$counted = $per_page*$page;
		}

		$v_data['page'] = $page;
		$v_data['visit_id'] = $visit_id;
		$page = $counted;
		$v_data['total_rows'] = $this->reception_model->count_items($table, $where);
		$query = $this->accounts_model->get_all_visits_invoice_items($table, $where, $config["per_page"], $page);



		$v_data['invoice_items'] = $query;

		$order = 'service_charge.service_charge_name';
		$where = 'service_charge.service_id = service.service_id AND service.service_name <> "Pharmarcy" AND service.service_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id = 1';

		$table = 'service_charge,visit_type,service';
		$config["per_page"] = 0;
		$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $procedure_query->result();
		$procedures = '';
		foreach ($rs9 as $rs10) :


		$procedure_id = $rs10->service_charge_id;
		$proced = $rs10->service_charge_name;
		$visit_type = $rs10->visit_type_id;
		$visit_type_name = $rs10->visit_type_name;

		$stud = $rs10->service_charge_amount;

		    $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

		endforeach;

		$v_data['services_list'] = $procedures;



		$order = 'service.service_name';
		$where = 'service.service_name <> "Pharmacy" AND service_status = 1';

		$table = 'service';
		$service_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $service_query->result();
		$services_items = '';
		foreach ($rs9 as $rs11) :


			$service_id = $rs11->service_id;
			$service_name = $rs11->service_name;

			$services_items .="<option value='".$service_id."'>".$service_name."</option>";

		endforeach;

		$v_data['services_items'] = $services_items;		// old
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();

		$this->load->view('view_bill',$v_data);

	}

	public function end_walkin_visits($visit_id)
	{
		$array = array('close_card' =>1);

		$this->db->where('visit_id',$visit_id);
		if($this->db->update('visit',$array))
		{
			$this->session->set_userdata('success_message','Visit was closed successfully');
		}
		else
		{
			$this->session->set_userdata('error_message','Sorry something went wrong please try again');
		}

		redirect('lab-walkins');
	}
}
?>