<!--<div class="center-align">
   <a href="<?php echo site_url().'doctor/print_checkup/'.$visit_id;?>" class="btn btn-danger btn-sm" target="_blank">Print Checkup</a>
</div>-->
<?php
   $exam_categories = $this->nurse_model->medical_exam_categories();
   
   if($exam_categories->num_rows() > 0)
   {
   	$exam_results = $exam_categories->result();
   	
   	foreach ($exam_results as $exam_res)
   	{
   		$mec_name = $exam_res->mec_name;
   		$mec_id = $exam_res->mec_id;
   		
   		$illnesses = $this->nurse_model->get_illness($visit_id, $mec_id);
   		
   		if($illnesses->num_rows() > 0)
   		{
   			$illnesses_row = $illnesses->row();
   			$mec_result= $illnesses_row->infor;
   		}
   		
   		else
   		{
   			$mec_result= '';
   		}
   		
   		if($mec_name=="Family History")
   		{
   			?>
            <div class="row">
                <div class="col-md-12">
                    <!-- Widget -->
                    <div class="widget boxed">
                        <!-- Widget head -->
                        <div class="widget-head">
                            <h4 class="pull-left"><i class="icon-reorder"></i>Family History</h4>
                            <div class="widget-icons pull-right">
                                <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                                <a href="#" class="wclose"><i class="icon-remove"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Widget content -->
                        <div class="widget-content">
                            <div class="padd">
                                <ol id="checkup_history"></ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
		}
   
		else if(($mec_name=="Further Details") || ($mec_name=="Conclusions")) 
		{
			?>
			<div class="row">
                <div class="col-md-12">
                    <div class="widget boxed">
                        <!-- Widget head -->
                        <div class="widget-head">
                            <h4 class="pull-left"><i class="icon-reorder"></i><?php echo $mec_name;?></h4>
                            <div class="widget-icons pull-right">
                                <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                                <a href="#" class="wclose"><i class="icon-remove"></i></a>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <!-- Widget content -->
                        <div class="widget-content">
                            <div class="padd">
                                <textarea class="cleditor" name="gg<?php echo $mec_id ?>"  id="gg<?php echo $mec_id ?>" placeholder="<?php echo $mec_name ?>" onKeyUp="save_illness('<?php echo $mec_id ?>','<?php echo $visit_id ?>')" ><?php echo $mec_result; ?></textarea>
                                <div class="row">
                                	<div class="col-md-4 col-md-offset-5">
                                    	<button type="button" onClick="save_illness('<?php echo $mec_id ?>','<?php echo $visit_id ?>')" class="btn btn-info">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php
       }
   
		else if(($mec_name=="ECOG Performance Index")||($mec_name=="General Examination")||($mec_name=="Eyes Examination")||($mec_name=="Mouth Examination")||($mec_name=="Neck Examination")||($mec_name=="Respiratory Examination")||($mec_name=="Cardiovascular Examination")||($mec_name=="Abdorminal/Pelvic Examination")||($mec_name=="Breast Examination")||($mec_name=="Nervous System")||($mec_name=="Groin and Gential")||($mec_name=="Skin Exam")||($mec_name=="Insurance Covers")||($mec_name=="Masculoskeletal System")||($mec_name=="Masculoskeletal System"))
		{	
			?>
            <div class="row">
                <div class="col-md-12">
                    <div class="widget boxed">
                        <!-- Widget head -->
                        <div class="widget-head">
                            <h4 class="pull-left"><i class="icon-reorder"></i><?php echo $mec_name;?></h4>
                            <div class="widget-icons pull-right">
                                <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                                <a href="#" class="wclose"><i class="icon-remove"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Widget content -->
                        <div class="widget-content">
                            <div class="padd">
                               <table class="table table-striped table-hover table-condensed">
                                    <?php
                                        $category_items = $this->nurse_model->mec_med($mec_id);
                                        
                                        if($category_items->num_rows() > 0)
                                        {
                                            $ab=0;
                                            $category_items_result = $category_items->result();
                                            
                                            foreach($category_items_result as $cat_res)
                                            {
                                                $item_format_id = $cat_res->item_format_id;
                                                $ab++;
                                                
                                                $cat_items = $this->nurse_model->cat_items($item_format_id, $mec_id);
                                                
                                                if($cat_items->num_rows() > 0)
                                                {
                                                    $cat_items_result = $cat_items->result();
                                                    
                                                    foreach($cat_items_result as $items_res)
                                                    {
                                                        $cat_item_name = $cat_item_name1 = $items_res->cat_item_name;
                                                        $cat_items_id1 = $items_res->cat_items_id;
                                                        $description = '';
                                                        $toggle = 0;
                                                        $style = 'block';
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $cat_item_name; ?> </td>
                                                            <?php
                                                                $items_cat = $this->nurse_model->get_cat_items($item_format_id, $mec_id);
                                                                
                                                                if($items_cat->num_rows() > 0)
                                                                {
                                                                    $items_result = $items_cat->result();
                                                                    
                                                                    foreach($items_result as $res)
                                                                    {
                                                                        $cat_item_name = $res->cat_item_name;
                                                                        $cat_items_id = $res->cat_items_id;
                                                                        $item_format_id1 = $res->item_format_id;
                                                                        $format_name = $res->format_name;
                                                                        $format_id = $res->format_id;
                                                                        
                                                                        if($cat_items_id == $cat_items_id1)
                                                                        {
                                                                            if($item_format_id1 == $item_format_id)
                                                                            {

                                                                                $results = $this->nurse_model->cat_items2($cat_items_id, $format_id,$visit_id);

                                                                                if($results->num_rows() > 0)
                                                                                {
                                                                                    if($results->num_rows() > 0)
                                                                                    {
                                                                                      $cat_result = $results->result();
                                                                        
                                                                                      foreach($cat_result as $res_two)
                                                                                      {
                                                                                        $description = $res_two->description;
                                                                                      }

                                                                                    }
                                                                                ?>
                                                                                <td > <input checked type="checkbox" value="" name="" onClick="medical_exam('<?php echo $cat_items_id; ?>','<?php echo $format_id ; ?>','<?php echo $visit_id; ?>')"/> <?php echo '<strong>'.$format_name.'</strong>'; ?>  </td>
                                                                                <?php 
                                                                                } 
                                                                                
                                                                                else 
                                                                                { 
                                                                                ?>
                                                                                <td> <input type="checkbox" value="" name="" onclick='toggleField("myTF<?php echo $cat_items_id; ?>");medical_exam("<?php echo $cat_items_id; ?>","<?php echo $format_id ; ?>","<?php echo $visit_id; ?>");' /> <?php echo '<strong>'.$format_name.'</strong>'; ?></td>
                                                                                <?php
                                                                                }
                                                                                
                                                                               
                                                                            }
                                                                        }	
                                                                        
                                                                    }
                                                                }
                                            
                                                                else
                                                                {
                                                                    echo 'There are no items';
                                                                }

                                                               $style = $this->nurse_model->get_if_checked($cat_items_id1, $visit_id);
                                                            ?>
                                                                <td> <textarea cols='20' name="myTF<?php echo $cat_items_id1;?>" id="myTF<?php echo $cat_items_id1;?>" rows='4' cols='1' style="display:<?php echo $style?>;"  onKeyUp='update_general_exam_detail("<?php echo $cat_items_id1 ?>","<?php echo $item_format_id; ?>","<?php echo 0; ?>","<?php echo $visit_id ?>" );' required placeholder='Describe <?php echo $cat_item_name1; ?>'><?php echo $description?> </textarea>
                                                                </td>
                                                                                
                                                            
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            
                                                else
                                                {
                                                    echo 'There are no category item results';
                                                }
                                            }
                                        }
                                            
                                        else
                                        {
                                            echo 'There are no category items';
                                        }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php	
   		} 
	} 
}
?>

<div class="row">
	<div class="col-md-12">
		
		<div class="form-group">
			<label class="col-lg-3 control-label">Medically Fit</label>
			<div class="col-lg-3">
				<div class="radio">
					<label>
					<input id="optionsRadios5" type="radio" value="1" name="medical_id">
					Yes
					</label>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="radio">
					<label>
					<input id="optionsRadios6" type="radio" value="2" name="medical_id" checked="checked">
					No
					</label>
				</div>
			</div>
		</div>
	</div>
</div>
<!--<div class="center-align" style="margin-top:10px;">
   <a href="<?php echo site_url().'doctor/print_checkup/'.$visit_id;?>" class="btn btn-danger btn-sm" target="_blank">Print Checkup</a>
</div>-->
<script type="text/javascript">
   var host = $('#config_url').val();

   function toggleField(field1) {



    var myTarget = document.getElementById(field1);

    if(myTarget.style.display == 'none'){
      myTarget.style.display = 'block';
        } else {
      myTarget.style.display = 'none';
      myTarget.value = '';
    }
  }
   
   function save_illness(mec_id, visit_id)
   {
   	var str1 = "gg";
   	var mec_id;
   	var n = str1.concat(mec_id); 
   	
   	//var patient_illness = document.getElementById(n).value;
	var patient_illness = tinyMCE.get(n).getContent();
	//alert(patient_illness);
   	var data_url = host+"nurse/save_illness/"+mec_id+"/"+visit_id;//alert(url);
   	
   	$.ajax({
   			type:'POST',
   			url: data_url,
   			data:{illness: patient_illness},
   			dataType: 'text json',
   			success:function(data){
   
   				//obj.innerHTML = XMLHttpRequestObject.responseText;
   			},
   			error: function(xhr, status, error) {
   				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   				alert(error);
   			}
   		});
   }
   
   function medical_exam(cat_items_id,format_id,visit_id){
   	
    var id= "myTF".concat(cat_items_id);
    var myTarget = document.getElementById(id);
    myTarget.style.display = 'block';


   	var XMLHttpRequestObject = false;
   		
   	if (window.XMLHttpRequest) {
   	
   		XMLHttpRequestObject = new XMLHttpRequest();
   	} 
   		
   	else if (window.ActiveXObject) {
   		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
   	}
   	
   	var url = host+"nurse/save_medical_exam/"+cat_items_id+"/"+format_id+"/"+visit_id;
   		//alert(url);
   	
   	if(XMLHttpRequestObject) {

   		//var obj = document.getElementById("insurance_company");
   				
   		XMLHttpRequestObject.open("GET", url);
   				
   		XMLHttpRequestObject.onreadystatechange = function(){
   			
   			if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
   				obj.innerHTML = XMLHttpRequestObject.responseText;
          
   			}
   		}
   				
   		XMLHttpRequestObject.send(null);
   	}
   }
   
   function del_medical_exam(cat_items_id,format_id,visit_id){
   	
   	var XMLHttpRequestObject = false;
   		
   	if (window.XMLHttpRequest) {
   	
   		XMLHttpRequestObject = new XMLHttpRequest();
   	} 
   		
   	else if (window.ActiveXObject) {
   		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
   	}
   	
   	var url = host+"nurse/delete_medical_exam/"+cat_items_id+"/"+format_id+"/"+visit_id;
   	alert(url);
   	
   	if(XMLHttpRequestObject) {
   		
   		//var obj = document.getElementById("insurance_company");
   				
   		XMLHttpRequestObject.open("GET", url);
   				
   		XMLHttpRequestObject.onreadystatechange = function(){
   			
   			if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
   				obj.innerHTML = XMLHttpRequestObject.responseText;
   			}
   		}
   				
   		XMLHttpRequestObject.send(null);
   	}
}

function update_general_exam_detail(cat_items_id,format_id,status,visit_id)
{
    var id= "myTF".concat(cat_items_id);
    var description = document.getElementById(id).value;
    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/general_exam/"+cat_items_id+"/"+format_id+"/"+status+"/"+visit_id;

  $.ajax({
    type:'POST',
    url: data_url,
    data:{description: description},
    dataType: 'text',
    success:function(data){
      // symptoms2(visit_id);
    },
    error: function(xhr, status, error) {
      //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
      alert(error);
    }
  });
  
}
</script>

