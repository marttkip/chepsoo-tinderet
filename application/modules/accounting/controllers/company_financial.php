<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/accounts/controllers/accounts.php";
error_reporting(1);
class Company_financial extends accounts 
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('company_financial_model');
		$this->load->model('inventory_management/inventory_management_model');
	}

	function profit_and_loss()
	{
		$search = $this->session->userdata('balance_sheet_search');
		if($search != 1)
		{
			$date_from = date('Y').'-01-01';
			$date_to = date('Y').'-12-31';
			$search_title = 'Statement for the year of '.date('Y').' ';

			$this->session->set_userdata('date_from_balance_sheet',$date_from);
			$this->session->set_userdata('date_to_balance_sheet',$date_to);
			$this->session->set_userdata('balance_sheet_title_search',$search_title);
			$this->session->set_userdata('balance_sheet_search',1);

		}

		$v_data['module'] = 1;
		$data['title'] = $v_data['title'] = ' Statement';
		$data['content'] = $this->load->view('reports/profit_and_loss', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	function balance_sheet()
	{
		$search = $this->session->userdata('balance_sheet_search');
		if($search != 1)
		{
			$date_from = date('Y').'-01-01';
			$date_to = date('Y').'-12-31';
			$search_title = 'Statement for the year of '.date('Y').' ';

			$this->session->set_userdata('date_from_balance_sheet',$date_from);
			$this->session->set_userdata('date_to_balance_sheet',$date_to);
			$this->session->set_userdata('balance_sheet_title_search',$search_title);
			$this->session->set_userdata('balance_sheet_search',1);

		}
		$v_data['module'] = 1;
		$data['title'] = $v_data['title'] = ' Balance Sheet';
		// var_dump($data); die();
		$data['content'] = $this->load->view('reports/balance_sheet', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function search_balance_sheet()
	{
		$date_from = $year_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		$redirect_url = $this->input->post('redirect_url');


		if(!empty($date_from) && !empty($date_to))
		{
			$date_from = $year_from.'-01-01';
			$date_to = $year_from.'-12-31';
			$search_title = 'REPORT FOR PERIOD '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		
		else if(!empty($date_from))
		{
			// $where .= ' AND creditor_account.creditor_account_date = \''.$date_from.'\'';

			$date_from = $year_from.'-01-01';
			// $search_title = 'REPORT FOR '.date('jS M Y', strtotime($date_from)).' ';
			$date_to = $year_from.'-12-31';
			$search_title = 'REPORT FOR PERIOD '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}
		
		else if(!empty($date_to))
		{
			$date_to = $date_to.'-01-01';
			// $where .= ' AND creditor_account.creditor_account_date = \''.$date_to.'\'';
			// $search_title = 'REPORT FOR '.date('jS M Y', strtotime($date_to)).' ';
			$date_to = $year_from.'-12-31';
			$search_title = 'REPORT FOR PERIOD '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}
		
		else
		{
			$date_from = date('Y').'-01-01';
			$date_to = date('Y').'-12-31';
			$search_title = 'Statement for the year of '.date('Y').' ';
		}
		
		// var_dump($date_to); die();
		$this->session->set_userdata('date_from_balance_sheet',$date_from);
		$this->session->set_userdata('date_to_balance_sheet',$date_to);
		$this->session->set_userdata('balance_sheet_title_search',$search_title);
		$this->session->set_userdata('balance_sheet_search',1);

		redirect($redirect_url);
		
	}

	public function close_balance_sheet_search()
	{
		$this->session->unset_userdata('date_from_balance_sheet');
		$this->session->unset_userdata('date_to_balance_sheet');
		$this->session->unset_userdata('balance_sheet_title_search');
		$this->session->unset_userdata('balance_sheet_search');
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);

	}

	public function search_visit_transactions($visit_type_id)
	{

		$visit_date_from = '';
		$visit_date_to = '';
		$search_status = $this->session->userdata('balance_sheet_search');
		if($search_status == 1)
		{
			$visit_date_from = $this->session->userdata('date_from_balance_sheet');
			$visit_date_to = $this->session->userdata('date_to_balance_sheet');
			
		}
		// var_dump($visit_date_to); die();
		$search_title = 'Showing reports for: ';
		
		if(!empty($visit_type_id))
		{
			$visit_type_id = ' AND visit.visit_type = '.$visit_type_id.' ';
			
			$this->db->where('visit_type_id', $visit_type_id);
			$query = $this->db->get('visit_type');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->visit_type_name.' ';
			}
		}		
	
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_payments = ' AND payments.payment_created BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$visit_invoices = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$date_from = $visit_date_from;
			$date_to = $visit_date_to;
			$cash_out = ' AND account_payments.payment_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$search_title .= 'Visit date from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_payments = ' AND payments.payment_created = \''.$visit_date_from.'\'';
			$visit_invoices = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$cash_out = ' AND account_payments.payment_date = \''.$visit_date_from.'\'';
			$search_title .= 'Visit date of '.date('jS M Y', strtotime($visit_date_from)).' ';
			$date_from = $visit_date_from;
			$date_to = '';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_payments = ' AND payments.payment_created = \''.$visit_date_to.'\'';
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$cash_out = ' AND account_payments.payment_date = \''.$visit_date_to.'\'';
			$search_title .= 'Visit date of '.date('jS M Y', strtotime($visit_date_to)).' ';
			$date_from = '';
			$date_to = $visit_date_to;
		}
		
		else
		{
			$visit_invoices = '';
			$visit_payments = '';
			$cash_out = '';
			$date_from = '';
			$date_to = '';
		}
		
		$search = $visit_type_id.$visit_invoices;
		// $visit_search = $this->session->userdata('all_transactions_search');		
		// var_dump($search); die();
		$this->session->set_userdata('cash_report_search', $search);
		$this->session->set_userdata('debtors_search_query', $search);
		$this->session->set_userdata('cash_out_search', $cash_out);
		$this->session->set_userdata('cash_report_date_from', $date_from);
		$this->session->set_userdata('cash_report_date_to', $date_to);
		$this->session->set_userdata('search_title', $search_title);
		$this->session->set_userdata('visit_invoices', $visit_invoices);
		$this->session->set_userdata('visit_payments', $visit_payments);
		
		// redirect('hospital-reports/cash-report');
		redirect('hospital-reports/all-transactions');
	}
	public function services_bills($service_id)
	{
		$where = 'visit_charge.visit_id = visit.visit_id AND visit.patient_id = patients.patient_id AND visit.visit_type = visit_type.visit_type_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit.visit_delete = 0 AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND service_charge.service_id = '.$service_id;

		$search = $this->session->userdata('search_hospital_creditors');
		
		$where .= $search;
		
		$table = 'visit_charge,visit,patients,service_charge,visit_type';
		$segment = 4;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'company-financials/services-bills/'.$service_id;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 40;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;

        $v_data["page"] = $page;
        $v_data['service_id'] = $service_id;
        $v_data["links"] = $this->pagination->create_links();
		$v_data['query'] = $this->company_financial_model->get_all_service_bills($table, $where, $config["per_page"], $page);
		$data['title'] = $v_data['title'] = 'Service Bills';
		$data['content'] = $this->load->view('reports/services_bills', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function export_services_bills($service_id)
	{
		// var_dump($service_id); die();
		$this->company_financial_model->export_services_bills($service_id);
	}
	function print_profit_and_loss()
	{
		$v_data['contacts'] = $this->site_model->get_contacts();
		$v_data['module'] = 1;
		$data['title'] = $v_data['title'] = ' Statement';
		$data['content'] = $this->load->view('reports/print_profit_and_loss_statement', $v_data);

	}
	public function print_balance_sheet()
	{
		$v_data['contacts'] = $this->site_model->get_contacts();
		$v_data['module'] = 1;
		$data['title'] = $v_data['title'] = ' Statement';
		$data['content'] = $this->load->view('reports/print_balance_sheet', $v_data);

	}

}
?>