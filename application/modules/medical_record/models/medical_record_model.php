<?php

class Medical_record_model extends CI_Model 
{
	public function get_leave_modifier($modified_by)
	{
		$personnel_fname = $personnel_onames = $personnel_name = '';
		$this->db->select('personnel_fname, personnel_onames');
		$this->db->where('personnel_id = '.$modified_by);
		$query = $this->db->get('personnel');
		
		if($query->num_rows() > 0)
		{
			$personnel_row = $query->row();
			$personnel_fname = $personnel_row->personnel_fname;
			$personnel_onames = $personnel_row->personnel_onames;
			
			$personnel_name = $personnel_fname.' '.$personnel_onames;
		}
		return $personnel_name;
	}
}
?>