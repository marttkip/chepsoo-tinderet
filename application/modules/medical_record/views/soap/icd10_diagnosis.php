<div class="row">
	<div class="col-md-12">
		
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">ICD10 Diagnosis</h2>
			</header>

			<div class="panel-body">
				<?php echo "
				<div class='center-align'>
					<input name='close' type='button' value='Close' class='btn btn-primary' onclick='close_window(".$visit_id.")' />
				</div>";
				?>
                <div class="row" style="padding:30px 0 30px 0;">
                    <div class="col-xs-8">
                        <select id='icd10_id' name='icd10_id' class="form-control custom-select">
                            <option value=''>None - Please Select a diagnosis</option>
                            <?php echo $icd10;?>
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <button type='submit' class="btn btn-sm btn-success"  onclick="pass_icd10_diagnosis(<?php echo $visit_id;?>);"> Identify</button>
                    </div>
                </div>
				<?php echo "
				<div class='center-align'>
					<input name='close' type='button' value='Close' class='btn btn-primary' onclick='close_window(".$visit_id.")' />
				</div>";
				?>   
            </div>
        </section>
    </div>
</div>
<?php echo form_close();?>

<script type="text/javascript">

$(function() {
});
$(document).ready(function(){

    $("#icd10_id").customselect();
});
	
	function close_window(visit_id)
	{
		window.close(this);
	}

	function pass_icd10_diagnosis(visit_id)
	{
	  var icd10_id = document.getElementById("icd10_id").value;
	  save_icd10(icd10_id, visit_id);
	}
	
	function save_icd10(val, visit_id)
	{
		var config_url = $('#config_url').val();
		var nav_link = config_url+"nurse/save_icd10/"+val+"/"+visit_id;
		$.get(nav_link, function( data ) 
		{
			get_disease(visit_id);
		});
	}
	
	function get_disease(visit_id)
	{
		var config_url = $('#config_url').val();
		var nav_link = config_url+"nurse/get_diagnosis/"+visit_id;
		$.get(nav_link, function( data ) 
		{
			$("#patient_diagnosis", window.opener.document).html(data);
		});
	}
</script>