<?php
$data['visit_id'] = $v_data['visit_id'] = $visit_id;
$data['lab_test'] = 100;

if(!isset($mobile_personnel_id))
{
	$mobile_personnel_id = NULL;
}
$v_data['mobile_personnel_id'] = $mobile_personnel_id;
$v_data['visit_history'] = 1;
//Symptoms
$rs_symptoms = $this->nurse_model->get_visit_symptoms($visit_id);
$num_rows_symptoms = count($rs_symptoms);

$v_data['signature_location'] = base_url().'assets/signatures/';
$v_data['query'] = $this->nurse_model->get_notes(3, $visit_id);

$symptoms = $this->load->view('medical_record/notes_history', $v_data, TRUE);

//Objective findings
//$rs_objective_findings = $this->nurse_model->get_visit_objective_findings($visit_id);
//$num_rows_objective_findings = count($rs_objective_findings);

//$objective_findings = $this->load->view('medical_record/notes_history', $v_data, TRUE);
$v_data['query'] = $this->nurse_model->get_notes(4, $visit_id);

$objective_findings = $this->load->view('medical_record/notes_history', $v_data, TRUE);

//Assessment
$v_data['query'] = $this->nurse_model->get_notes(5, $visit_id);

$assessment = $this->load->view('medical_record/notes_history', $v_data, TRUE);

//Assessment
//Plan
$v_data['query'] = $this->nurse_model->get_visit_pan_detail($visit_id);
$plan = $this->load->view('medical_record/plan', $v_data, TRUE);





$v_data['query'] = $this->nurse_model->get_notes(7, $visit_id);

$presenting_complain = $this->load->view('medical_record/notes_history', $v_data, TRUE);

$v_data['query'] = $this->nurse_model->get_notes(8, $visit_id);

$review = $this->load->view('medical_record/notes_history', $v_data, TRUE);

//Plan
$v_data['query'] = $this->nurse_model->get_visit_pan_detail($visit_id);

$plan = $this->load->view('medical_record/plan', $v_data, TRUE);

?>
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Patient card</h2>
		<a class="btn btn-sm btn-info pull-right" style="margin-top:-25px;" href="<?php echo site_url()?>patient-cards/<?php echo $patient_id;?>"><i class="fa fa-arrow-left"></i> Back to patient cards</a>
	</header>
	
	<!-- Widget content -->
	
	<div class="panel-body">
		
		<div class="well well-sm info">
			<h5 style="margin:0;">
				<div class="row">
					<div class="col-md-2">
						<div class="row">
							<div class="col-lg-6">
								<strong>First name:</strong>
							</div>
							<div class="col-lg-6">
								<?php echo $patient_surname;?>
							</div>
						</div>
					</div>
					
					<div class="col-md-3">
						<div class="row">
							<div class="col-lg-6">
								<strong>Other names:</strong>
							</div>
							<div class="col-lg-6">
								<?php echo $patient_othernames;?>
							</div>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="row">
							<div class="col-lg-6">
								<strong>Gender:</strong>
							</div>
							<div class="col-lg-6">
								<?php echo $gender;?>
							</div>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="row">
							<div class="col-lg-6">
								<strong>Age:</strong>
							</div>
							<div class="col-lg-6">
								<?php echo $age;?>
							</div>
						</div>
					</div>
					
					<div class="col-md-3">
						<div class="row">
							<div class="col-lg-6">
								<strong>Account balance:</strong>
							</div>
							<div class="col-lg-6">
								Kes <?php echo number_format($account_balance, 2);?>
							</div>
						</div>
					</div>
				</div>
			</h5>
		</div>
        
        <div class="center-align">
          	<?php
              	$error = $this->session->userdata('error_message');
              	$validation_error = validation_errors();
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($validation_error))
				{
					echo '<div class="alert alert-danger">'.$validation_error.'</div>';
				}
				
				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
      		?>
            <?php echo $this->load->view("nurse/allergies_brief", '', TRUE);?>
			<div class="clearfix"></div>
        </div>
		<div class="row">
            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Visit vitals</h2>
                    </header>
                    <div class="panel-body">
                        <div id="previous_vitals"></div>
                    </div>
                </section>
            </div>
        </div>


         <div class="row">
            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Diagnosis</h2>
                    </header>
                    <div class="panel-body">
                        <?php echo $this->load->view("medical_record/diagnosis_history", $data, TRUE); ?>
                    </div>
                </section>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12"> 
        
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Procedures</h2>
                    </header>
                    <div class="panel-body">
                        <div id="procedures"></div>
                    </div>
                 </section>
            </div>
        </div>
          <div class="row">
            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Presenting Complaint</h2>
                    </header>
        
                    <div class="panel-body">
                    	<div class='row'>
                            <div class='col-md-4'>
                                <h4>Selected Complain</h4>
								<?php
                                //symptoms table
								echo '<div id="visit_symptoms1">';
                                if($num_rows_symptoms > 0)
								{
                                    echo"<table class='table table-striped table-condensed table-bordered'>"; 
                                        echo"<tr>"; 
                                            echo"<th>";
                                                echo"#"; 
                                            echo"</th>"; 
                                            echo"<th>";
                                                echo"Symptom"; 
                                            echo"</th>"; 
                                            echo"<th>";
                                                echo"Yes/ No"; 
                                            echo"</th>"; 
                                            echo"<th>";
                                                echo"Description"; 
                                            echo"</th>"; 
                                        echo"</tr>"; 
                                            $count=0;
                                            foreach ($rs_symptoms as $key):	
                                                $count++;
                                                $symptoms_name = $key->symptoms_name;
                                                $status_name = $key->status_name;
                                                $visit_symptoms_id = $key->visit_symptoms_id;
                                                $description= $key->description;
                                                
                                                echo"<tr>"; 
                                                    echo"<td>";
                                                        echo $count; 
                                                    echo"</td>"; 
                                                    echo"<td>";
                                                        echo $symptoms_name; 
                                                    echo"</td>"; 
                                                    echo"<td>";
                                                        echo $status_name; 
                                                    echo"</td>"; 
                                                    echo"<td>";
                                                        echo $description; 
                                                    echo"</td>"; 
                                                echo"<tr>"; 
                                            endforeach;
                                            
                                            echo "
                                            </table>
                                        ";
                                }
                                
                                else
                                {
                                    echo '<p>No complain selected</p>';
                                }
                                echo "</div>";
                                
                                ?>
                            </div>
                            
                            <div class="col-md-8">
                                <h4>Other Complains</h4>
                                <?php echo $symptoms;?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
         <div class="row">

            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Presenting Illness HistORY</h2>
                    </header>
        
                    <div class="panel-body">
                        <?php echo $objective_findings;?>
                    </div>
                </section>
            </div>
        </div>
     
        <div class="row">
            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Past Medical History</h2>
                    </header>
        
                    <div class="panel-body">
                        <?php echo $objective_findings;?>

            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Presenting Illness History</h2>
                    </header>
        
                    <div class="panel-body">
                        <?php echo $presenting_complain;?>

                    </div>
                </section>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Family Social History</h2>


         <div class="row">
            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Past Medical History</h2>

                    </header>
        
                    <div class="panel-body">
                        <?php echo $objective_findings;?>
                    </div>
                </section>
            </div>
        </div>

         <div class="row">
            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title"></h2>

      <div class="row">
            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Family Social History</h2>

                    </header>
        
                    <div class="panel-body">
                        <?php echo $assessment;?>
                    </div>
                </section>
            </div>
        </div>
         <div class="row">
        
       
        
        <div id="lab_test_results_section"></div>>
        
        <div class="row">
            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Plan</h2>
                    </header>
                    
                    <div class="panel-body">
                        <?php echo $plan;?>
                    </div>
                </section>
            </div>
        </div>
        
        <div class="row">
          	<div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Prescription</h2>
                    </header>
                    <div class="panel-body">
                        <div id="visit_prescription"></div>
                    </div>
                      
                 </section>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
	var config_url = $('#config_url').val();
	var visit_id = '<?php echo $visit_id;?>';
	
	$(document).ready(function()
	{
		previous_vitals(visit_id);
		display_procedure(visit_id);
		display_visit_vaccines(visit_id);
		display_visit_consumables(visit_id);
		get_lab_results(visit_id);
		display_prescription(visit_id);
	});
	
	function previous_vitals(visit_id)
	{
		var url = config_url+"nurse/previous_vitals/"+visit_id;
		$.get(url, function( data ) 
		{
			$("#previous_vitals").html(data);
		});
	}
	
	function display_procedure(visit_id)
	{
		var url = config_url+"nurse/view_procedure/"+visit_id;
		$.get(url, function( data ) 
		{
			$("#procedures").html(data);
		});
	}
	
	function display_visit_vaccines(visit_id)
	{
		var url = config_url+"nurse/visit_vaccines/"+visit_id;
		$.get(url, function( data ) 
		{
			$("#vaccines_to_patients").html(data);
		});
	}
	
	function display_visit_consumables(visit_id)
	{
		var url = config_url+"nurse/visit_consumables/"+visit_id;
		$.get(url, function( data ) 
		{
			$("#consumables_to_patients").html(data);
		});
	}
	
	function get_lab_results(visit_id)
	{
		var nav_link = config_url+"medical_record/get_lab_test_results/"+visit_id;
		$.get(nav_link, function( data ) 
		{
			$("#lab_test_results_section").html(data);
		});
	}

	function display_prescription(visit_id)
	{
		var config_url = $('#config_url').val();
		var url = config_url+"pharmacy/display_prescription/"+visit_id;
		$.get(url, function( data ) 
		{
			$("#visit_prescription").html(data);
		});
	}
</script>